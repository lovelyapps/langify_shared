/* {%- comment -%}Version 0.2.0{%- endcomment -%} */
var langify = langify || {};

/**
 * 
 * 
 * @class Helper
 */
class Helper {
  static ajax(params) {
    var data = params.data;
    fetch(params.url, {
      method: params.method,
      headers: {
        'Content-Type': 'application/json',
      },
      body: (data && params.method === 'POST') ? JSON.stringify(data) : null,
    })
    .then(response => response.json())
    .then(data => {
      params.success(data);
    })
    .catch((error) => {
      console.error('Error:', error);
    });
  }
  static isIE() {
    var ua = navigator.userAgent;
    var is_ie = ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1;
    return is_ie;
  }
  static extractImageObject(val) {
    if(!val || val == '') return false;
    var val = val;

    // Handle src-sets
    if(val.search(/([0-9]+w?h?x?,)/gi) > -1) {
      val = val.split(/([0-9]+w?h?x?,)/gi)[0];
    }

    var url = val;
    var host = file = name = type = '';
    url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));
    url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));
    url = url.substring(url.lastIndexOf("/") + 1, url.length);
    var hostBegin = val.indexOf('//') ? val.indexOf('//') : 0;
    host = val.substring(hostBegin, val.lastIndexOf('/') + 1);
    name = url.replace(/(_[0-9]+x[0-9]*|_{width}x|_{size})?(_crop_(top|center|bottom|left|right))?(@[0-9]*x)?(\.progressive)?\.(jpe?g|png|gif|webp)/gi, "");
    type = url.substring(url.lastIndexOf('.')+1, url.length);
    file = url.replace(/(_[0-9]+x[0-9]*|_{width}x|_{size})?(_crop_(top|center|bottom|left|right))?(@[0-9]*x)?(\.progressive)?\.(jpe?g|png|gif|webp)/gi, '.'+type);
    
    return {
      host: host,
      name: name,
      type: type,
      file: file
    }
  }
  static getCurrentLanguage() {
    return window.langify.locale.iso_code;
  }
  static setLanguage(code) {
    
  }
  static getCookie(name) {
    var v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
    if(v) return v[2];
    else return null;
  }
  static setCookie(name, value, days) {
    var d = new Date;
    d.setTime(d.getTime() + 24*60*60*1000*days);
    document.cookie = name + "=" + value + ";path=/;expires=" + d.toGMTString();
  }
  static getVersion() {
    return 2;
  }
  static isOutOfViewport (elem) {
    var bounding = elem.getBoundingClientRect();
    var out = {};
    out.top = bounding.top < 0;
    out.left = bounding.left < 0;
    out.bottom = Math.ceil(bounding.bottom) >= (window.innerHeight || document.documentElement.clientHeight);
    out.right = bounding.right > (window.innerWidth || document.documentElement.clientWidth);
    out.any = out.top || out.left || out.bottom || out.right;
    out.inViewport = bounding.x > 0 && bounding.y > 0;
    return out;
  }
  static isDomainFeatureEnabled() {
    return window.langify.locale.domain_feature_enabled;
  }
  static getVal(str) {
    var v = window.location.search.match(new RegExp('(?:[?&]'+str+'=)([^&]+)'));
    return v ? v[1] : null;
  }
  static inIframe() {
    try {
      return window.self !== window.top;
    } catch (e) {
      return true;
    }
  }
  static shopifyAPI() {
    var root_url = window.langify.locale.root_url != '/' ? window.langify.locale.root_url : '';

    return {
      attributeToString: function(attribute) {
        if((typeof attribute) !== 'string') {
          attribute += '';
          if(attribute === 'undefined') {attribute = '';}
        }
        return attribute.trim();
      },
      getCart: function(callback) {
        Helper.ajax({
          method: 'GET',
          url: root_url+'/cart.js',
          success: function (cart, textStatus) {
            if((typeof callback) === 'function') {
              callback(cart);
            }
          }
        });
      },
      updateCartNote: function(note, callback) {
        var params = {
          method: 'POST',
          url: root_url+'/cart/update.js',
          data: 'note=' + this.attributeToString(note),
          dataType: 'json',
          success: function(cart) {if((typeof callback) === 'function') {callback(cart);}},
          error: this.onError
        };
        Helper.ajax(params);
      },
      updateCartAttributes: function(attributes, callback) {
        var params = {
          method: 'POST',
          url: root_url+'/cart/update.js',
          data: {"attributes": attributes},
          dataType: 'json',
          success: function(cart) {
            if((typeof callback) === 'function') {
              callback(cart);
            }
          },
          error: this.onError
        };
        Helper.ajax(params);
      },
      onError: function(XMLHttpRequest, textStatus) {

      }      
    }
  }
  static loadScript(url, callback) {
    var script = document.createElement('script');
    script.type = 'text/javascript';
    if(script.readyState) {
      script.onreadystatechange = function () {
        if(script.readyState == 'loaded' || script.readyState == 'complete') {
          script.onreadystatechange = null;
          callback();
        }
      };
    } else {
      script.onload = function () {
        callback();
      };
    }
    script.src = url;
    document.getElementsByTagName('head')[0].appendChild(script);
  }
  static localizationRedirect(type, code, additionalField, additionalParams) {
    if(type !== 'country_code' && type !== 'language_code') { 
      return false; 
    }
    if(!additionalParams) {
      var additionalParams = '';
    }
    var params = [
      {
        name: type,
        value: code
      },
      {
        name: 'return_to',
        value: window.location.pathname + window.location.search + additionalParams + window.location.hash
      },
      {
        name: 'form_type',
        value: 'localization'
      },
      {
        name: '_method',
        value: 'put'
      }
    ];
    if(additionalField) {
      params = Object.assign(params, additionalField);
    }
    const form = document.createElement('form');
    form.method = 'POST';
    form.action = '/localization';
    params.forEach(function(param) {
      const field = document.createElement('input');
      field.type = 'hidden';
      field.name = param['name'];
      field.value = param['value'];
      form.appendChild(field);
    });
    document.body.appendChild(form);
    form.submit();
  }
  static changeCurrency(code) {
    alert(code)
    const params = [
      {
        name: 'currency_code',
        value: code
      },
      {
        name: 'return_to',
        value: window.location.pathname + window.location.search + window.location.hash
      },
      {
        name: 'form_type',
        value: 'localization'
      },
      {
        name: '_method',
        value: 'put'
      }
    ];
    const form = document.createElement('form');
    form.method = 'POST';
    form.action = '/localization';
    params.forEach(function(param) {
      const field = document.createElement('input');
      field.type = 'hidden';
      field.name = param['name'];
      field.value = param['value'];
      form.appendChild(field);
    });
    document.body.appendChild(form);
    form.submit();
  }
};


/**
 *
 *
 * @class TranslationObserver
 */
class TranslationObserver {
  constructor() {
    console.log('TranslationObserver instantiation')
  }

  init() {
    if(!Helper.isIE()){
      var langifyObserver = this.langifyObserverCore();
      langifyObserver.init();

      langify.api = {
        observer: {
          start: langifyObserver.init,
          stop: langifyObserver.stopObserver,
          trigger: langifyObserver.triggerCustomContents
        }
      };
    }

    return this;
  }

  langifyObserverCore = function() {
    var observedNodes = [];
    var intersectionObs = null;
    var mutationCount = 0;
    var mutationObs = null;
    var mutationObsConfig = {
      characterData: true,
      characterDataOldValue: true,
      attributes: true,
      attributeOldValue: true,
      childList: true,
      subtree: true,
    };
    var customContents_html = {};
    var customContents_text = {};
    var customContents_attr = {};
  
    function init() {
      if(langify.settings.observe) {
        spreadCustomContents();
        if (window.MutationObserver) {
          if (langify.settings.lazyload && window.IntersectionObserver) {
            startIntersectionObserver();
          } else {
            startMutationObserver(null);
          }
        } else {
          startMutationEvents();
        }
      }
    }
  
  
    // Intersection Observer
    function startIntersectionObserver() {
      intersectionObs = new IntersectionObserver(callbackIntersectionChange);
      var elementNodes = document.getElementsByClassName('shopify-section');
      Array.prototype.slice.call(elementNodes).forEach(function(node) {
        intersectionObs.observe(node);
      });
    }
  
    function callbackIntersectionChange(intersections) {
      intersections.forEach(function(intersection) {
        var target = intersection.target;
        var targetIsIntersecting = Boolean(target.getAttribute('ly-is-intersecting') === "true" ? true : false);
  
        if(typeof targetIsIntersecting == 'undefined' || targetIsIntersecting == null) {
          target.setAttribute('ly-is-intersecting', intersection.isIntersecting);
          targetIsIntersecting = intersection.isIntersecting;
        }
        
        // On Screen
        if(targetIsIntersecting == false && intersection.isIntersecting == true) {
          if(!target.getAttribute('ly-is-observing')) {
            startMutationObserver(target);
            intersectionObs.unobserve(target);
  
            // As the mutation observer didn't see the "add" of the elememts, trigger the initial translation manually
            var elementNodes = getNodesUnder(target, 'SHOW_ELEMENT');
            var textNodes = getNodesUnder(target, 'SHOW_TEXT');
            var nodes = elementNodes.concat(textNodes);
            nodes.forEach(function(node) {
              translate(node, 'mutation -> childList');
            });
          }
        } 
        // Off Screen
        //else if(targetIsIntersecting == true && intersection.isIntersecting == false) {
        //  stopMutationObserver(target);
        //}
        target.setAttribute('ly-is-intersecting', intersection.isIntersecting);
      });
    }
  
  
    // Mutation Observer
    function startMutationObserver(observerTarget) {
      if (!observerTarget) {
        var target = document.documentElement || document.body; // main window
      } else {
        if (observerTarget.nodeName  === 'IFRAME') {
          var target = observerTarget.contentDocument || observerTarget.contentWindow.document; // iframe
        } else {
          var target = observerTarget;
        }
      }
      mutationObs = new MutationObserver(callbackDomChange);
      mutationObs.observe(target, mutationObsConfig);
      observedNodes.push(target);
      try { target.setAttribute('ly-is-observing', 'true'); } catch(e) {}
    }
  
    function stopMutationObserver(target) {
      if(observedNodes.indexOf(target) > -1) {
        observedNodes.splice(observedNodes.indexOf(target), 1);
      }
      var mutations = mutationObs.takeRecords();
      mutationObs.disconnect();
      observedNodes.forEach(node => {
        mutationObs.observe(node, mutationObsConfig);
      });
      try { target.removeAttribute('ly-is-observing'); } catch(e) {}
    }
  
  
    function callbackDomChange(mutations, mutationObs) {
      for (var i = 0, length = mutations.length; i < length; i++) {
        var mutation = mutations[i];
        var target = mutation.target;
  
        // Links-Hook
        if (mutation.type === 'attributes' && (mutation.attributeName === 'href' || mutation.attributeName === 'action')) {
          var target = mutation.target;
          translateLink(target.getAttribute(mutation.attributeName), target);
        }
        // Images-Hook
        else if (mutation.type === 'attributes' && (mutation.attributeName === 'src' || mutation.attributeName === 'data-src' || mutation.attributeName === 'srcset' || mutation.attributeName === 'data-srcset')) {
          translateImage(target, mutation.attributeName);
        }
        else if (mutation.type === 'attributes' && mutation.attributeName === 'style') {
          translateCssImage(target);
        }
        // Subtree Events
        else if (mutation.type === 'childList') {
          // Added Nodes
          if(mutation.addedNodes.length > 0) {
            var elementNodes = getNodesUnder(mutation.target, 'SHOW_ELEMENT');
            var textNodes = getNodesUnder(mutation.target, 'SHOW_TEXT');
            var nodes = elementNodes.concat(textNodes);
            nodes.forEach(function(node) {
              translate(node, 'mutation -> childList');
            });
          }
        }
        // CharacterData Events
        else if (mutation.type === 'characterData') {
          var target = mutation.target;
          translate(target, 'mutation -> characterData');
        }
      }
    }
  
  
    function translate(node, info) {
      if((node.nodeType === 1 && node.hasAttribute('data-ly-locked')) || (node.nodeType === 3 && node.parentNode && node.parentNode.hasAttribute('data-ly-locked'))) {
        return;
      }
      // CC-Hook
      if (node.nodeName  !== 'SCRIPT' && node.nodeName  !== 'STYLE') {
        if (node.nodeType === 3) {
          translateTextNode(node, info);
        }
        translateNodeAttrs(node);
      }
      // Links-Hook
      if (node.nodeName  === 'A' || node.nodeName  === 'FORM') {
        if(node.hasAttribute('href')) var attrName = 'href';
        else var attrName = 'action';
        var url = node.getAttribute(attrName);
        translateLink(url, node);
      }
      // Images-Hook
      if (node.nodeName  === 'IMG' || node.nodeName  === 'SOURCE') {
        translateImage(node, ['src', 'data-src', 'srcset', 'data-srcset']);
      }
      if (node.attributes && node.getAttribute('style')) {
        translateCssImage(node);
      }
      // Iframe Observation
      if (node.nodeName  === 'IFRAME') {
        // Todo: handle srcdoc iframe content observing
        if (node.getAttribute('ly-is-observing') == null && node.getAttribute('src') == null && !node.hasAttribute('srcdoc')) {
          node.setAttribute('ly-is-observing', 'true');
          startMutationObserver(node);
        }
      }
    }
  
    function translateNodeAttrs(node) {
      if(Object.keys(customContents_attr).length > 0) {
  
      }
    }
  
  
    function translateTextNode(node, info) {
      if(langify.settings.observeCustomContents === false || !node.textContent || node.textContent.trim().length === 0) {
        return;
      }
      var src = node.textContent.trim().replace(/(\r\n|\n|\r)/gim,"").replace(/\s+/g," ").toLowerCase();
      if(customContents_text[src] && node.textContent !== customContents_text[src]) {
        var newContent = node.textContent.replace(node.textContent.trim(), customContents_text[src]);
        if (newContent != node.textContent) {
          if(!node.parentNode.hasAttribute('data-ly-mutation-count') || parseInt(node.parentNode.getAttribute('data-ly-mutation-count')) < langify.settings.maxMutations) {
            var count = node.parentNode.hasAttribute('data-ly-mutation-count') ? parseInt(node.parentNode.getAttribute('data-ly-mutation-count')) : 0;
            node.parentNode.setAttribute('data-ly-mutation-count', count+1);
            node.textContent = newContent;
  
            mutationCount = mutationCount + 1;
            log('REPLACED (TEXT)', {
                oldValue: src,
                newValue: customContents_text[src],
                mutationCount,
              }, 'success'
            );
          
            var event = new CustomEvent('langify.observer.aftertranslatetext', { 
              bubbles: true,
              detail: {
                target: node,
                original: src,
                translation: customContents_text[src]
              }
            });
            node.dispatchEvent(event);
          }
        }
      }
    }
  
    function translateLink(url, node) {
      if(langify.settings.observeLinks === false || isLocalizationForm(node) || node.hasAttribute('data-ly-locked') || !url || (url.indexOf('mailto:') !== -1 || url.indexOf('javascript:') !== -1 || url.indexOf('tel:') !== -1 || url.indexOf('file:') !== -1 || url.indexOf('ftp:') !== -1 || url.indexOf('sms:') !== -1 || url.indexOf('market:') !== -1 || url.indexOf('fax:') !== -1 || url.indexOf('callto:') !== -1 || url.indexOf('ts3server:') !== -1)) {
        return;
      }
      var isLocalized = (url.indexOf('/'+ langify.locale.iso_code +'/') === 0 || url === '/'+ langify.locale.iso_code || url.indexOf('/'+ langify.locale.iso_code.toLowerCase() +'/') === 0 || url === '/'+ langify.locale.iso_code.toLowerCase());
      var cleanUrl = url.replace(langify.locale.shop_url, '');
      var re = new RegExp(`\/\\b${langify.locale.iso_code.replace('-', '\-')}\\b\/`, 'gi');
      var link = cleanUrl.replace(re, '/');
      var isUrlAbsolute = (link.indexOf('://') > 0 || link.indexOf('//') === 0);
      var blacklist = ['#', '/'+ langify.locale.iso_code +'#'].concat(langify.settings.linksBlacklist);
      var isUrlBlacklisted = blacklist.find(x => url.indexOf(x) === 0);
      if(!isLocalized && !isUrlAbsolute && !isUrlBlacklisted && langify.locale.root_url != '/') {
        if(node.hasAttribute('href')) var attrName = 'href'; else var attrName = 'action';
        if(link === '/' || link == langify.locale.root_url) link = '';
        var newLink = langify.locale.root_url + link;
        var timeStamp = Math.floor(Date.now());
        if(!node.hasAttribute('data-ly-processed') || timeStamp > parseInt(node.getAttribute('data-ly-processed')) + langify.settings.timeout) {
          node.setAttribute('data-ly-processed', timeStamp);
          node.setAttribute(attrName, newLink);
          if(node.hasAttribute('data-'+attrName)) node.setAttribute('data-'+attrName, newLink);
  
          mutationCount = mutationCount + 1;
          log('REPLACED (LINK)', {
              attrName,
              oldValue: url,
              newValue: newLink,
              mutationCount,
            }, 'success'
          );
          
          var event = new CustomEvent('langify.observer.aftertranslatelink', { 
            bubbles: true,
            detail: {
              target: node,
              attribute: attrName,
              original: url,
              translation: newLink
            }
          });
          node.dispatchEvent(event);
        }
      }
    }
  
    function translateImage(node, attr) {
      if(langify.settings.observeImages === false || node.hasAttribute('data-ly-locked') || Object.keys(customContents_image).length === 0) {
        return
      }
      var attrs = [];
      if(typeof attr === 'string') attrs.push(attr);
      else if(typeof attr === 'object') attrs = attr;
      var timeStamp = Math.floor(Date.now());
      if(!node.hasAttribute('data-ly-processed') || timeStamp > parseInt(node.getAttribute('data-ly-processed')) + langify.settings.timeout) {
        node.setAttribute('data-ly-processed', timeStamp);
        attrs.forEach(function(attr) {
          if(node.hasAttribute(attr)) {
            var imgObject = Helper.extractImageObject(node.getAttribute(attr));
            var imgKey = imgObject ? imgObject.file.toLowerCase() : '';
  
            if(customContents_image[imgKey]) {
  
              // Replace
              var oldValue = node.getAttribute(attr);
              var translation = node.getAttribute(attr);
              translation = translation.replace(new RegExp(imgObject.host, 'g'), customContents_image[imgKey].host);
              translation = translation.replace(new RegExp(imgObject.name, 'g'), customContents_image[imgKey].name);
              translation = translation.replace(new RegExp(imgObject.type, 'g'), customContents_image[imgKey].type);
  
              // China Hook
              if(node.getAttribute(attr).indexOf('cdn.shopifycdn.net') >= 0) {
                translation = translation.replace(new RegExp('cdn.shopify.com', 'g'), 'cdn.shopifycdn.net'); 
              }
  
              if(node.getAttribute(attr) != translation) {
                node.setAttribute(attr, translation);
  
                mutationCount = mutationCount + 1;
                log('REPLACED (IMAGE)', {
                  attrName: attr,
                  oldValue: oldValue,
                  newValue: translation,
                  mutationCount,
                }, 'success');
          
                var event = new CustomEvent('langify.observer.aftertranslateimage', { 
                  bubbles: true,
                  detail: {
                    target: node,
                    attribute: attr,
                    original: oldValue,
                    translation: translation
                  }
                });
                node.dispatchEvent(event);
              }
            }
          }
        });
      }
    }
  
    function translateCssImage(node) {
      if(langify.settings.observeImages === false || node.hasAttribute('data-ly-locked') || Object.keys(customContents_image).length === 0 || !node.getAttribute('style')) {
        return
      }
  
      var imgMatches = node.getAttribute('style').match(/url\(("|')?(.*)("|')?\)/gi);
      if(imgMatches !== null) {
        var imgSource = imgMatches[0].replace(/url\(("|')?|("|')?\)/, '');
        var imgObject = Helper.extractImageObject(imgSource);
        var imgKey = imgObject ? imgObject.file.toLowerCase() : '';
        var attr = 'style';
  
        if(customContents_image[imgKey]) {
  
          // Replace
          var translation = node.getAttribute(attr);
          translation = translation.replace(new RegExp(imgObject.host, 'g'), customContents_image[imgKey].host);
          translation = translation.replace(new RegExp(imgObject.name, 'g'), customContents_image[imgKey].name);
          translation = translation.replace(new RegExp(imgObject.type, 'g'), customContents_image[imgKey].type);
  
          // China Hook
          if(node.getAttribute(attr).indexOf('cdn.shopifycdn.net') >= 0) {
            translation = translation.replace(new RegExp('cdn.shopify.com', 'g'), 'cdn.shopifycdn.net'); 
          }
  
          if(node.getAttribute(attr) != translation) {
            var timeStamp = Math.floor(Date.now());
            if(!node.hasAttribute('data-ly-processed') || timeStamp > parseInt(node.getAttribute('data-ly-processed')) + langify.settings.timeout) {
              node.setAttribute('data-ly-processed', timeStamp);
              node.setAttribute(attr, translation);
              mutationCount = mutationCount + 1;
            }
          }
        }          
      }
    }
  
  
  
    function findAndLocalizeLinks(target, parent) {
      if(target.parentNode && parent) var allLinks = target.parentElement.querySelectorAll('[href],[action]');
      else var allLinks = target.querySelectorAll('[href],[action]');
      allLinks.forEach(link => {
        if(link.hasAttribute('href')) var attrName = 'href'; else var attrName = 'action';
        var url = link.getAttribute(attrName);
        translateLink(url, link);
      });
    }
    
    function isLocalizationForm(node) {
      if(node.querySelector('input[name="form_type"][value="localization"]')){
        return true;
      }
      return false;
    }
  
    function getNodesUnder(el, show){
      var n, a=[], walk=document.createTreeWalker(el, NodeFilter[show] , null, false);
      while(n=walk.nextNode()) a.push(n);
      return a;
    }
  
    function log(title, data, type) {
      if(langify.settings.debug) {
        let css = 'color: green; font-weight: bold;';
        console.log(`%c ${title}: \n`, css, data);
      }
    }
  
    function spreadCustomContents() {
      var getFileName = function(url) {
        if(!url || url == '') return;
        url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));
        url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));
        url = url.substring(url.lastIndexOf("/") + 1, url.length);
        url = url.replace(/(_[0-9]+x[0-9]*|_{width}x)?(_crop_(top|center|bottom|left|right))?(@[0-9]*x)?(\.progressive)?\.(jpe?g|png|gif|webp)/gi, "");
        return '/' + url;
      }
      Object.entries(customContents).forEach(function(entry) {
        //if(/<\/?[a-z][\s\S]*>/i.test(entry[0])) customContents_html[entry[0]] = entry[1];
        //else if(/(http(s?):)?([/|.|\w|\s|-])*\.(?:jpe?g|gif|png)/.test(entry[0])) customContents_image[getFileName(entry[0])] = getFileName(entry[1]);
        if(/(http(s?):)?([/|.|\w|\s|-])*\.(?:jpe?g|gif|png|webp)/.test(entry[0])) customContents_image[Helper.extractImageObject(entry[0]).file] = Helper.extractImageObject(entry[1]);
        else customContents_text[entry[0]] = entry[1];
      });
      log('CUSTOM CONTENTS:', {customContents, customContents_text, customContents_image}, 'info')
    }
  
    // Polyfill for old browsers
    function startMutationEvents() {
      var target = document.querySelector('body');
      target.addEventListener("DOMAttrModified", function (event) {
        if(event.attrName === 'href' || event.attrName === 'action') {
          if(event.prevValue != event.newValue) {
            translateLink(event.newValue, event.target);
          }
        }
      }, false);
      target.addEventListener("DOMSubtreeModified", function (event) {
        //findAndLocalizeLinks(event.target, false);
        //matchCustomContent(event.target);
      }, false);
    }
  
    function triggerCustomContents() {
      var rootnode = document.getElementsByTagName('body')[0]
      var walker = document.createTreeWalker(rootnode, NodeFilter.SHOW_ALL, null, false)
      
      while (walker.nextNode()) {
        //console.log(walker.currentNode.tagName)
        translate(walker.currentNode, null);
      }
    }
  
    function stopObserver() {
      mutationObs.takeRecords();
      mutationObs.disconnect();
    }
  
    //init();
    return {
      init: init,
      triggerCustomContents: triggerCustomContents,
      stopObserver: stopObserver,
    }
  };
}


/**
 *
 * 
 * @class Switcher
 */
class Switcher {
  constructor(switcherSettings) {
    console.log('Switcher instantiation');

    this.lyForceOff = location.search.split('ly-force-off=')[1];
    this.switcherElements = Array.prototype.slice.call(document.getElementsByClassName('ly-switcher-wrapper'));
  }

  init() {

    // Initial cart.attributes update
    Helper.shopifyAPI().getCart(function (cart) {
      var currentLanguage = langify.locale.iso_code;
      if(!cart.attributes.language || cart.attributes.language != currentLanguage) {
        Helper.shopifyAPI().updateCartAttributes({"language": currentLanguage}, function(data){});
      }
    });

    if(langify.locale.languages.length === 1) {
      console.info(`%c LANGIFY INFO:\n`, 'font-weight: bold;', 'The language switcher is hidden! This can have one of the following reasons: \n * All additional languages are disabled in the "Langify -> Dashboard -> Languages" section. \n * If you are using different domains for your additional languages, ensure that "Cross Domain Links" are enabled inside the "Langify -> Dashboard -> Switcher Configurator -> Domains" section.');
      return false;
    }

    this.initCustomDropdown();
    this.bindCountrySwitchers();
    this.bindCurrencySwitchers();
    this.bindLanguageSwitchers();
    this.setCustomPosition();

    var event = new CustomEvent('langify.switcher.initialized', { 
      bubbles: true,
      detail: {}
    });
  }

  initCustomDropdown() {
    // Custom Dropdown
    var root = this;
    var switcher = document.getElementsByClassName('ly-custom-dropdown-switcher');
    for(var a = 0; a < switcher.length; a++) {
      switcher[a].classList.toggle('ly-is-open');
      var isOut = Helper.isOutOfViewport(switcher[a]);
      if(isOut.bottom && isOut.inViewport) {
        switcher[a].classList.add('ly-is-dropup');
        var arrows = switcher[a].querySelectorAll('.ly-arrow');
        for(var b = 0; b < arrows.length; b++) {
          arrows[b].classList.add('ly-arrow-up');    
        }
      }
      switcher[a].classList.toggle('ly-is-open');
      switcher[a].onclick = function(event) {
        root.toggleSwitcherOpen(this);
      }
    }

    document.addEventListener('click', function(event) {
      if(!event.target.closest('.ly-custom-dropdown-switcher')) {
        var openSwitchers = document.querySelectorAll('.ly-custom-dropdown-switcher.ly-is-open')
        for(var i = 0; i < openSwitchers.length; i++) {
          openSwitchers[i].classList.remove('ly-is-open');
        }
      }
    });
  }
  
  bindLanguageSwitchers() {
    var links = document.getElementsByClassName('ly-languages-switcher-link');
    for(var l = 0; l < links.length; l++) {
      links[l].addEventListener('click', function(event) {
        Helper.setCookie('ly-lang-selected', this.getAttribute('data-language-code'), 365);
        Helper.localizationRedirect('language_code', this.getAttribute('data-language-code'));
      });
    }

    // Native select event handling
    var nativeLangifySelects = document.getElementsByClassName('ly-native-select');
    for(var i = 0; i < nativeLangifySelects.length; i++) {
      nativeLangifySelects[i].onchange = function() {
        var selectedLanguageCode = this[this.selectedIndex].getAttribute('key');
        Helper.setCookie('ly-lang-selected', selectedLanguageCode, 365);
        Helper.localizationRedirect('language_code', this.getAttribute('data-language-code'));
      };
    }
  }
  
  bindCountrySwitchers() {
    var currencySelectSwitcher = document.querySelectorAll('select.ly-country-switcher');
    var currencyCustomSwitcher = document.querySelectorAll('div.ly-country-switcher a');
    for(var i=0; i<currencyCustomSwitcher.length; i++) {
      currencyCustomSwitcher[i].addEventListener('click', function(e) {
        Helper.localizationRedirect('country_code', this.getAttribute('data-country-code'));
      });
    }
    for(var a=0; a<currencySelectSwitcher.length; a++) {
      currencySelectSwitcher[a].addEventListener('change', function(){
        Helper.localizationRedirect('country_code', this.getAttribute('data-country-code'));
      });
    }  
  }
  
  bindCurrencySwitchers() {
    var currencySelectSwitcher = document.querySelectorAll('select.ly-currency-switcher');
    var currencyCustomSwitcher = document.querySelectorAll('div.ly-currency-switcher a');
    for(var i=0; i<currencyCustomSwitcher.length; i++) {
      currencyCustomSwitcher[i].addEventListener('click', function(e) {
        Helper.changeCurrency(this.getAttribute('data-currency-code'));
      });
    }
    for(var a=0; a<currencySelectSwitcher.length; a++) {
      currencySelectSwitcher[a].addEventListener('change', function(){
        Helper.changeCurrency(this.getAttribute('data-currency-code'));
      });
    }  
  }

  setCustomPosition() {
    for(var i = 0; i < this.switcherElements.length; i++) {
      if(this.lyForceOff !== 'true') {
        if(langify.locale.languages.length <= 1) {
          this.switcherElements[i].querySelector('.ly-languages-switcher').classList.add('ly-hide');
        }
        this.switcherElements[i].classList.remove('ly-hide');
      }
      if(this.switcherElements[i].classList.contains('ly-custom') && (document.getElementById('ly-custom-'+this.switcherElements[i].getAttribute('data-breakpoint')) || document.getElementsByClassName('ly-custom-'+this.switcherElements[i].getAttribute('data-breakpoint')).length )) {
        var targets = Array.from(document.getElementsByClassName('ly-custom-'+this.switcherElements[i].getAttribute('data-breakpoint')));
        var target = document.getElementById('ly-custom-'+this.switcherElements[i].getAttribute('data-breakpoint'));
        if(target) targets.push(target);
        for(var c = 0; c < targets.length; c++) {
          var clone = this.switcherElements[i].cloneNode(true);
          targets[c].innerHTML = clone.outerHTML;
        }
        this.switcherElements[i].classList.add('ly-hide');
      }
    }
  }

  toggleSwitcherOpen(e) {
    var target = e;
    if(!target.classList.contains('ly-is-open')) {
      var openSwitchers = document.querySelectorAll('.ly-custom-dropdown-switcher.ly-is-open')
      for(var i = 0; i < openSwitchers.length; i++) {
        openSwitchers[i].classList.remove('ly-is-open');
      }
    }
    target.classList.toggle('ly-is-open');
    var isOut = Helper.isOutOfViewport(target);
    if(isOut.bottom) {
      target.classList.add('ly-is-dropup');
    }
  }

  togglePopupOpen(e) {
    e.closest('.ly-popup-switcher').classList.toggle('ly-is-open');
  }
}


/**
 *
 *
 * @class LanguageDetection
 */
class LanguageDetection {
  constructor() {
    console.log('LanguageDetection instantiation')
  }

  init() {
    var currentLang = langify.locale.iso_code;
    var localizationData = this.getLocalizationData();

    var userLang = navigator.language || navigator.userLanguage;
    var bot = /bot|google|baidu|bing|msn|duckduckbot|teoma|slurp|yandex|Chrome-Lighthouse/i.test(navigator.userAgent);
    var blockedRoutes = window.lyBlockedRoutesList || [];
    var blockedRoute = blockedRoutes.find(x => window.location.pathname.indexOf(x) === 0);
    var is404 = document.getElementsByClassName('template-404').length;

    if(Helper.getVal('redirected') === 'true') {
      Helper.setCookie('ly-lang-selected', langify.locale.iso_code, 365);
      var _href = window.location.href;
      history.replaceState(null, '', _href.replace(/(\?|&)(redirected=true)/i, ''));

      return null;
    }

    if(!Helper.inIframe() && !is404 && !bot && !blockedRoute && Helper.getVal('ly-lang-detect') !== 'off') {
      if(userLang && !Helper.getCookie('ly-lang-selected')) {
        if(currentLang !== userLang) {
          Helper.setCookie('ly-lang-selected', userLang, 365);
          this.redirect(localizationData);
        }
      } else {
        if(Helper.getCookie('ly-lang-selected') && Helper.getCookie('ly-lang-selected') !== currentLang) {

          // Only save cookie when the domain feature is active
          if(Helper.isDomainFeatureEnabled()) {
            Helper.setCookie('ly-lang-selected', currentLang, 365);
          }
          this.redirect(localizationData);
        }
      }
    }

    this._geolocationAppSupport();
  }
  redirect(localizationData) {
    var additionalField = {};
    var additionalParams = '?redirected=true';
    if(location.search.indexOf('?') >= 0) {
      additionalParams = '&redirected=true';
    }
    if(localizationData.country_code) {
      additionalField = {
        name: 'country_code',
        value: localizationData.country_code
      }
    }
    Helper.localizationRedirect('language_code', localizationData.language_code, additionalField, additionalParams);
  }
  getLocalizationData() {
    var browserLang = navigator.language || navigator.userLanguage;
    var localization = browserLang.split('-');
    var data = {};
    data.language_code = localization[0];
    if(localization.length === 2) {
      data.country_code = localization[1];
    }
    return data;
  }
  _geolocationAppSupport() {
    var target = document.querySelector('body');
    var config = { childList: true, subtree: true };
    var geolocationObserver = new MutationObserver(function(mutations) {
      mutations.forEach(function(mutation) {
        if (mutation.type === 'childList' && mutation.addedNodes.length > 0) {
          var target = mutation.target.getElementsByClassName('locale-bar__form');
          if(target[0]) {
            target[0].onsubmit = function() {
              var locale_code = target[0].elements['locale_code'].value;
              var selector = target[0].getElementsByClassName('locale-bar__selector');
              if(selector.length >= 1) {
                locale_code = selector[0].options[selector[0].selectedIndex].value;
              }
              Helper.setCookie('ly-lang-selected', locale_code, 365);
            };
          }
          var selectors = mutation.target.getElementsByClassName('locale-selectors__selector');
          if(selectors.length > 0) {
            for(var selector of selectors) {
              if(selector.getAttribute('name') === 'locale_code') {
                selector.onchange = function() {
                  var locale_code = selector.options[selector.selectedIndex].value;
                  Helper.setCookie('ly-lang-selected', locale_code, 365);
                };        
              }
            }
          }
        }
      });
    });
    geolocationObserver.observe(target, config);
    setTimeout(function() {
      geolocationObserver.disconnect();
    }, 10000);
  }
}


/**
 *
 *
 * @class Recommendation
 */
class Recommendation extends LanguageDetection {
  constructor() {
    super();
    console.log('Recommendation instantiation')
  }

  init() {
    console.log(this.getLocalizationData());
  }




  
  recommendation = function() {
    return {
      recommendated: null,
      el: null,
      init: function() {
        var _localizationForm = document.getElementById('currency_switcher_form')
        this.el = {
          localizationForm: _localizationForm,
          currencyCodeField: _localizationForm.querySelector('input[name="currency_code"]'),
          countryCodeField: _localizationForm.querySelector('input[name="country_code"]'),
          languageCodeField: _localizationForm.querySelector('input[name="language_code"]'),
        };
        var recommendated = this._findRecommendedLanguage();
        this.recommendated = recommendated;
        if(recommendated && recommendated !== langify.locale.iso_code && !Helper.getCookie('ly-lang-selected')) {
          this._translateStrings(recommendated);
          this._bindings();
        } else {
          return false;
        }
      },
      _findRecommendedLanguage: function() {
        var currentLang = langify.locale.iso_code;
        var browserLang = navigator.language || navigator.userLanguage;
        var match = languageUrls[browserLang];
        var recommendatedLang = false;
        if(!match) {
          browserLang = browserLang.substring(0, 2);
          match = languageUrls[browserLang];
        }
        if(!match) {
          for(var lang in languageUrls) {
            if(lang.substring(0, 2) === browserLang) {
              browserLang = lang;
              match = languageUrls[browserLang];
            recommendatedLang = browserLang;
            }
          }
        } else {
          recommendatedLang = browserLang;
        }
        return recommendatedLang;
      },
      _translateStrings: function(languageCode) {
        var recommendationElement = document.querySelectorAll('.ly-recommendation')[0];
        var strings = langify.settings.switcher.recommendation_strings[languageCode];
        var recommendationStr = 'Looks like your browser is set to English. Change the language?';
        var buttonStr = 'Change';
        var languageStr = 'English';
        if(strings) {
          recommendationStr = strings.recommendation ? strings.recommendation : 'Looks like your browser is set to English. Change the language?';
          buttonStr = strings.button ? strings.button : 'Change';
          languageStr = strings.language ? strings.language : 'English';
        }
        var newCode = recommendationElement.innerHTML;
        newCode = newCode.replaceAll('[[recommendation]]', recommendationStr).replaceAll('[[button]]', buttonStr);
        recommendationElement.innerHTML = newCode;     
      },
      _bindings: function() {
        var _this = this;
        var recommendated = this._findRecommendedLanguage();
        var currentLang = langify.locale.iso_code;
        var userLang = navigator.language || navigator.userLanguage;
        var lyForceOff = location.search.split('ly-force-off=')[1];
    
    
        var recommendationElement = document.querySelectorAll('.ly-recommendation')[0];
        var form = recommendationElement.querySelectorAll('.ly-recommendation-form')[0];
        var links = recommendationElement.getElementsByClassName('ly-custom-dropdown-list-element');
        var customDropdown = recommendationElement.querySelectorAll('.ly-custom-dropdown-switcher');
        var nativeSelects = recommendationElement.getElementsByClassName('ly-native-select');
    
        //form.setAttribute('action', languageUrls[this.recommendated]);
        if(!this.el.languageCodeField) {
          // Backwards compability
          this.el.languageCodeField = document.createElement('input');
          this.el.languageCodeField.type = 'hidden';
          this.el.languageCodeField.name = 'language_code';
          this.el.localizationForm.appendChild(this.el.languageCodeField)
        }
        this.el.languageCodeField.value = recommendated;
    
        form.addEventListener('submit', function(e) {
          e.preventDefault();
          Helper.setCookie('ly-lang-selected', recommendated, 365);
          _this.el.localizationForm.submit();
        });
        
        for(var l = 0; l < links.length; l++) {
          links[l].addEventListener('click', function(event) {
            event.preventDefault();
            
            // When clicked from within a custom dropdown
            var parentCustomDropdown = event.currentTarget.closest('.ly-custom-dropdown-switcher');
            if(parentCustomDropdown) {
              _this._selectCustomDropdownEntry(parentCustomDropdown, this);
            } else {
              _this.el.languageCodeField.value = this.getAttribute('data-language-code');
            }
          });
        }
    
        for(var i = 0; i < nativeSelects.length; i++) {
          nativeSelects[i].onchange = this._selectNativeDropdownEntry;//.bind(this);
          
          if(nativeSelects[i].classList.contains('ly-languages-switcher')) {
            nativeSelects[i].value = recommendated;
          }
        }
    
        for(var i = 0; i < customDropdown.length; i++) {
          if(recommendationElement.className.indexOf('bottom') > -1) {
            customDropdown[i].classList.add('ly-is-dropup');       
          }
          
          if(customDropdown[i].classList.contains('ly-languages-switcher')) {
            this._selectCustomDropdownEntry(customDropdown[i], customDropdown[i].querySelector('.ly-custom-dropdown-list a[data-language-code="'+ recommendated +'"]'));
          }
        } 
        if(!lyForceOff) {
          recommendationElement.classList.add('ly-is-open');
        }
      },
    
      _selectNativeDropdownEntry: function(event) {
        event.preventDefault();
        var element = event.currentTarget;
        var _this = langify.recommendation;
    
        if(element.classList.contains('ly-languages-switcher')) {
          _this.el.languageCodeField.value = element[this.selectedIndex].getAttribute('key');
          //element.value = valueToSelect;  
        }
        if(element.classList.contains('ly-country-switcher') || element.classList.contains('ly-currency-switcher')) {
          _this.el.currencyCodeField.value = element[this.selectedIndex].getAttribute('data-currency');
          _this.el.countryCodeField.value = element[this.selectedIndex].getAttribute('data-country-code');
        }
      },
      _selectCustomDropdownEntry: function(element, entry) {
        var _this = langify.recommendation;
        var currentElem = element.querySelector('.ly-custom-dropdown-current');
        var currentIcon = currentElem.querySelector('.ly-icon');        
        var currentLabel = currentElem.querySelector('span');
        var valueToSelect = '';
    
        if(element.classList.contains('ly-languages-switcher')) {
          _this.el.languageCodeField.value = entry.getAttribute('data-language-code');
          valueToSelect = entry.getAttribute('data-language-code');
        }
        if(element.classList.contains('ly-country-switcher')) {
          _this.el.currencyCodeField.value = entry.getAttribute('data-currency');
          _this.el.countryCodeField.value = entry.getAttribute('data-country-code');
          valueToSelect = entry.getAttribute('data-country-code')+'-'+entry.getAttribute('data-currency');
        }
        if(element.classList.contains('ly-currency-switcher')) {
          _this.el.currencyCodeField.value = entry.getAttribute('data-currency');
          _this.el.countryCodeField.value = '';
          valueToSelect = entry.getAttribute('data-currency');
        }
    
        if(element.querySelector('.ly-custom-dropdown-list li.current')) {
          element.querySelector('.ly-custom-dropdown-list li.current').classList.remove('current');  
          element.querySelector('.ly-custom-dropdown-list li[key="'+ valueToSelect +'"]').classList.add('current');  
        }
        if(element.querySelector('.ly-custom-dropdown-list li.current span')) currentLabel.innerHTML = element.querySelector('.ly-custom-dropdown-list li.current span').innerHTML;
        if(element.querySelector('.ly-custom-dropdown-list li.current .ly-icon')) currentIcon.classList = element.querySelector('.ly-custom-dropdown-list li.current .ly-icon').classList;
      },
      toggleOpen: function(e) {
        e.closest('.ly-recommendation').classList.toggle('ly-is-open');
        if(!e.closest('.ly-recommendation').classList.contains('ly-is-open')) {
          Helper.setCookie('ly-lang-selected', this.recommendated, 365);
        }
      },
    }
  }
}


/**
 *
 *
 * @class Langify
 */
class Langify {
  constructor(settings, locale) {
    console.log('Langify instantiation')
    console.dir(settings)
    console.dir(locale)

    var translationObserver = new TranslationObserver().init();


    var lyForceOff = location.search.split('ly-force-off=')[1];
    if(lyForceOff === 'true') {
      document.getElementById('preview-bar-iframe').classList.add('ly-hide');
      return false;
    }

    if(settings.theme && ((settings.theme.loadJquery && settings.theme.loadJquery === true) || typeof settings.theme.loadJquery === 'undefined')) {
      if(typeof jQuery === 'undefined') {
        Helper.loadScript('//cdn.jsdelivr.net/jquery/1.9.1/jquery.min.js', function() {});
      } 
    }

    //document.addEventListener("DOMContentLoaded", function() {
      var switcher = new Switcher().init();

      if(settings.switcher) {
        if(settings.switcher.recommendation && settings.switcher.recommendation_enabled) {
          var recommendation = new Recommendation().init();
        } else {
          if(settings.switcher.languageDetection && parseInt(settings.switcher.version.replaceAll('.', '')) >= 300) {
            var languageDetection = new LanguageDetection().init();
          }        
        }
      }
    //});
  }
}