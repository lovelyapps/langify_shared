<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

define("APP_DIR", realpath(__DIR__ . "/../"));

require APP_DIR . '/vendor/autoload.php';
require APP_DIR . '/SharedFiles/MyDBConnection.php';

$config = new MyDBConnection();
$pdo = $config->getDB();

function getVersionDb($pdo, $hash, $filename) {
    $stmt = $pdo->prepare("SELECT version FROM file_versions WHERE hash = ? AND filename = ?");
    $stmt->execute([$hash, $filename]);
    $result = $stmt->fetch();

    if (!$result) {
        $stmt = $pdo->prepare("SELECT max(version) as version FROM file_versions WHERE filename = ? GROUP BY filename");
        $stmt->execute([$filename]);
        $result = $stmt->fetch();

        if (!$result) {
            $version = 1;
        } else {
            $version = $result['version'] + 1;
        }
        $pdo->query("INSERT INTO file_versions (hash, filename, version) VALUES ('$hash', '$filename', '$version')");
        return strval($version);
    }

    return $result['version'];
}

function getVersionByContent($pdo, $hash, $filename, $withDots = true) {
    if($filename == "langify_media.zip") return getenv('LANGIFY_MEDIA_ZIP');
    $fileWithPath = APP_DIR . "/public/" . $filename;
    $content = file_get_contents($fileWithPath);
    $re = '/{%- comment -%}Version (?<major>\d*).(?<minor>\d*).(?<build>\d*){%- endcomment -%}/m';
    if(preg_match_all($re, $content, $matches, PREG_SET_ORDER, 0)) {
        if(isset($matches[0]["major"]) && isset($matches[0]["minor"]) && isset($matches[0]["build"])) {
            if($withDots) {
                return $matches[0]["major"] . "." . $matches[0]["minor"] . "." . $matches[0]["build"];
            } else {
                return $matches[0]["major"] . $matches[0]["minor"];
            }
        }
    }

    return getVersionDb($pdo, $hash, $filename);
}

$fsFiles = glob('*');
$files = [];
foreach ($fsFiles as $file) {
    if(substr_count($file, "ly-") > 0 || substr_count($file, "langify_") > 0) {
        $files[] = [
            'name' => $file,
            'version' => getVersionByContent($pdo, hash_file('md5', $file), $file)
        ];
    }
}

$output['files'] = $files;
$output['settings'] = [
    'url' => $config->getConfig('LANGIFY_BACKEND_API_URL')
];

header('Content-Type: application/json');
echo json_encode($output);