<?php
require '../vendor/autoload.php';
define("APP_DIR", realpath(__DIR__ . "/../"));

$dbFile = APP_DIR . '/data/db.sqlite';
if(!file_exists($dbFile)) {
    copy(APP_DIR . 'db.sqlite.init', $dbFile);
}

$pdo = new PDO( 'sqlite:' . $dbFile, '', '', array(
    \PDO::ATTR_EMULATE_PREPARES => true,
    \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
    \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC
));

$sql = 'SELECT * FROM updates';
$stmt = $pdo->query($sql);

$rawArray = $stmt->fetchAll(PDO::FETCH_ASSOC);
$result = [];
foreach($rawArray as $row) {
    $result[$row['insert_date']] = ["token" => $row['string_token'], "result" => count($row['result'])];
}

header('Content-Type: application/json');
echo json_encode($result);
