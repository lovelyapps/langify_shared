<?php

$dotenv = Dotenv\Dotenv::createUnsafeImmutable("..");
$dotenv->safeLoad();

class MyDBConnection
{
    private \PDO $db;

    public function __construct()
    {
        $dsn = getenv('DB_DSN');
        $mysqlUser = getenv('DB_USER');
        $mysqlPass = getenv('DB_PASS');
        $this->db = new PDO($dsn, $mysqlUser, $mysqlPass, array(
            \PDO::ATTR_EMULATE_PREPARES => true,
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC
        ));
    }

    public function getConfig($name, $default = false) {
        return getenv($name) ?: $default;
    }

    public function getDB(): \PDO
    {
        return $this->db;
    }
}