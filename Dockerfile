FROM composer as builder
WORKDIR /app/
COPY composer.* ./
RUN composer install --verbose --no-progress --no-interaction --no-dev --optimize-autoloader --ignore-platform-reqs --no-scripts

FROM webdevops/php-nginx:7.4
ENV WEB_DOCUMENT_ROOT=/var/www/public

COPY --chown=application:application ./ /var/www/
COPY --chown=application:application --from=builder /app/vendor /var/www/vendor

COPY bin/startup.sh /entrypoint.d/startup.sh