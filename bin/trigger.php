#!/usr/bin/env php
<?php

sleep(30);

define("APP_DIR", realpath(__DIR__ . "/../"));

require APP_DIR . '/vendor/autoload.php';
require APP_DIR . '/SharedFiles/MyDBConnection.php';

$token = uniqid();
$now = date_create("now")->format("Y-m-d.His");
touch(APP_DIR . "/data/{$token}.{$now}.lock");
echo "Token: {$token}\n";

$dbFile = APP_DIR . '/data/db.sqlite';
if(!file_exists($dbFile)) {
    copy(APP_DIR . 'db.sqlite.init', $dbFile);
}

echo "open db\n";

$db = new MyDBConnection();
$pdo = $db->getDB();

$sql = 'INSERT INTO trigger_log (single_token) VALUES (:single_token)';
$stmt = $pdo->prepare($sql);
$stmt->bindValue(':single_token', $token);
$stmt->execute();

// create curl resource
$ch = curl_init();

// set url
curl_setopt($ch, CURLOPT_URL, $db->getConfig('LANGIFY_BACKEND_API_URL'));

curl_setopt($ch, CURLOPT_POSTFIELDS,
    "single_token=" . $token);

//return the transfer as a string
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

// curl_setopt($ch, CURLOPT_VERBOSE, 1);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

// $output contains the output string
$output = curl_exec($ch);

echo "request with output: {$output}\n";

$sql = 'UPDATE trigger_log SET result = :result WHERE single_token = :single_token';
$stmt = $pdo->prepare($sql);
$stmt->bindValue(':single_token', $token);
$stmt->bindValue(':result', $output);
$stmt->execute();


// close curl resource to free up system resources
curl_close($ch);

echo "ok\n";